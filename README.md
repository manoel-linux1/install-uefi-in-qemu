# install-uefi-in-qemu

- Note: that the OVMF binary is from Arch Linux and was not compiled by me

- install-uefi-in-qemu-version: dec 2023

- build-latest: 0.0.5

- install-uefi-in-qemu is an open-source project, and we are happy to share it with the community. You have complete freedom to do whatever you want with install-uefi-in-qemu, in accordance with the terms of the MIT license. You can modify, distribute, use it in your own projects, or even create a fork of install-uefi-in-qemu to add additional features.

## Installation

- To install install-uefi-in-qemu, follow the steps below:

# 1. Clone this repository by running the following command

- git clone https://gitlab.com/manoel-linux1/install-uefi-in-qemu.git

# 2. To install the install-uefi-in-qemu script, follow these steps

- chmod a+x `installupdate.sh`

- sudo `./installupdate.sh`

# For check version

sudo `ovmf-check-version` or `ovmf-check-version`

# For uninstall

- chmod a+x `uninstall.sh`

- sudo `./uninstall.sh`

# Other Projects

- If you found this project interesting, be sure to check out my other open-source projects on GitLab. I've developed a variety of tools and scripts to enhance the Linux/BSD experience and improve system administration. You can find these projects and more on my GitLab: https://gitlab.com/manoel-linux1

# Project Status

- The install-uefi-in-qemu project is currently in development. The latest stable version is 0.0.5. We aim to provide regular updates and add more features in the future.

# License

- install-uefi-in-qemu is licensed under the MIT License. See the LICENSE file for more information.

# Acknowledgements

- We would like to thank the open-source community for their support and the libraries used in the development of install-uefi-in-qemu.
