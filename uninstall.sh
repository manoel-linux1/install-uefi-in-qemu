#!/bin/bash

clear
echo "#################################################################"
echo "(install-uefi-in-qemu-uninstaller) >> (dec 2023)"
echo "#################################################################"
echo " ██    ██ ███████ ███████ ██ "
echo " ██    ██ ██      ██      ██ "
echo " ██    ██ █████   █████   ██ "
echo " ██    ██ ██      ██      ██ "
echo "  ██████  ███████ ██      ██ "    
echo "#################################################################"
echo "(install-uefi-in-qemu-gitlab) >> (https://gitlab.com/manoel-linux1/install-uefi-in-qemu)"
echo "#################################################################"

if [[ $EUID -ne 0 ]]; then
echo " ███████ ██████  ██████   ██████  ██████  ██ "
echo " ██      ██   ██ ██   ██ ██    ██ ██   ██ ██ "
echo " █████   ██████  ██████  ██    ██ ██████  ██ "
echo " ██      ██   ██ ██   ██ ██    ██ ██   ██    "
echo " ███████ ██   ██ ██   ██  ██████  ██   ██ ██ "                                                                                        
echo "#################################################################"
echo "(Superuser privileges or sudo required to execute the script)" 
echo "#################################################################"
exit 1
fi

clear

sudo rm /usr/share/ovmf-github-x86_64/OVMF.fd

sudo rm -rf /usr/share/ovmf-github-x86_64/

sudo rm /usr/share/ovmf-gitlab-x86_64/OVMF.fd

sudo rm -rf /usr/share/ovmf-gitlab-x86_64/

sudo rm /usr/share/ovmf-github-ia_32/OVMF.fd

sudo rm -rf /usr/share/ovmf-github-ia_32/

sudo rm /usr/share/ovmf-gitlab-ia_32/OVMF.fd

sudo rm -rf /usr/share/ovmf-gitlab-ia_32/

sudo rm /usr/bin/ovmf-check-version

clear

echo "#################################################################"
echo " ██████   ██████  ███    ██ ███████ ██ "
echo " ██   ██ ██    ██ ████   ██ ██      ██ "
echo " ██   ██ ██    ██ ██ ██  ██ █████   ██ "
echo " ██   ██ ██    ██ ██  ██ ██ ██         "
echo " ██████   ██████  ██   ████ ███████ ██ " 
echo "#################################################################"
echo "(Uninstallation completed)"
echo "#################################################################"
